from django.apps import AppConfig


class DocumenttrackerConfig(AppConfig):
    name = 'DocumentTracker'
