from django.apps import AppConfig


class QualityDashboardConfig(AppConfig):
    name = 'Quality_Dashboard'
